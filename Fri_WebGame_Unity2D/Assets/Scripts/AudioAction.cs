﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AudioAction : MonoBehaviour, IPointerClickHandler
{

    public Sprite audioPlaySprite;
    public Sprite audioUnPlaySprite;
    AudioSource music;

    void Awake() 
    {
        music = GameObject.Find("BGM").GetComponent<AudioSource>();
         if(music.isPlaying)
        {
            gameObject.GetComponent<Image>().sprite = audioPlaySprite;
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = audioUnPlaySprite;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(music.isPlaying)
        {
            gameObject.GetComponent<Image>().sprite = audioUnPlaySprite;
            music.Pause();
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = audioPlaySprite;
            music.UnPause();
        }
        
    }
}
