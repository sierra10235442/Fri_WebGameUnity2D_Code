﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DonDestroyObject : MonoBehaviour
{
    static DonDestroyObject _instance;
    public static DonDestroyObject Instance {get {return _instance;}}
    
    void Awake() 
    {
        if(_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }    
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        SceneManager.LoadScene("Menu");
    }
    void Start()
    {
        
    }
    void Update()
    {
        
    }
}
